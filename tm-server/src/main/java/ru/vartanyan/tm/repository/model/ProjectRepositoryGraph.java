package ru.vartanyan.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.model.ProjectGraph;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepositoryGraph extends AbstractRepository<ProjectGraph>
        implements IProjectRepositoryGraph {

    public ProjectRepositoryGraph(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    public @NotNull ProjectGraph findOneById(@Nullable final String id) {
        return entityManager.find(ProjectGraph.class, id);
    }

    @Override
    public ProjectGraph findOneByName(@Nullable final String userId, @Nullable final String name) {
        return getSingleResult(entityManager
                .createQuery(
                        "SELECT e FROM ProjectGraph e WHERE e.name = :name AND e.user.id = :userId",
                        ProjectGraph.class
                )
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1));

    }

    public void removeOneById(@Nullable final String id) {
        ProjectGraph reference = entityManager.getReference(ProjectGraph.class, id);
        entityManager.remove(reference);
    }

    public void remove(@NotNull final ProjectGraph entity) {
        ProjectGraph reference = entityManager.getReference(ProjectGraph.class, entity.getId());
        entityManager.remove(reference);
    }

    @NotNull
    public List<ProjectGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectGraph e", ProjectGraph.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAllByUserId(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectGraph e WHERE e.user.id = :userId", ProjectGraph.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull ProjectGraph findOneByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return getSingleResult(entityManager
                .createQuery(
                        "SELECT e FROM ProjectGraph e WHERE e.id = :id AND e.user.id = :userId", ProjectGraph.class
                )
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1));
    }

    @Override
    public @NotNull ProjectGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        return getSingleResult(entityManager
                .createQuery("SELECT e FROM ProjectGraph e WHERE e.user.id = :userId", ProjectGraph.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1));
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e")
                .executeUpdate();
    }

    @Override
    public void removeOneByIdAndUserId(@Nullable final String userId, @NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.user.id = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
