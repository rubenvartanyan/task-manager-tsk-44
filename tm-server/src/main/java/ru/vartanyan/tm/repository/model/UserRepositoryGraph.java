package ru.vartanyan.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.model.UserGraph;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryGraph extends AbstractRepository<UserGraph>
        implements IUserRepositoryGraph {

    public UserRepositoryGraph(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<UserGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM UserGraph e", UserGraph.class).getResultList();
    }

    @Override
    public @NotNull UserGraph findOneById(@Nullable final String id) {
        return entityManager.find(UserGraph.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserGraph e")
                .executeUpdate();
    }

    public void removeOneById(@Nullable final String id) {
        UserGraph reference = entityManager.getReference(UserGraph.class, id);
        entityManager.remove(reference);
    }

    @Override
    public @NotNull UserGraph findByLogin(@Nullable final String login) {
        List<UserGraph> list = entityManager
                .createQuery("SELECT e FROM UserGraph e WHERE e.login = :login", UserGraph.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        return list.get(0);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserGraph e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

}

