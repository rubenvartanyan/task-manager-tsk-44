package ru.vartanyan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.dto.IUserRepository;
import ru.vartanyan.tm.dto.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM UserGraph e", User.class).getResultList();
    }

    @Override
    public @NotNull User findOneById(@Nullable final String id) {
        return entityManager.find(User.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserGraph e")
                .executeUpdate();
    }

    public void removeOneById(@Nullable final String id) {
        User reference = entityManager.getReference(User.class, id);
        entityManager.remove(reference);
    }

    @Override
    public @NotNull User findByLogin(@Nullable final String login) {
        return getSingleResult(entityManager
                .createQuery("SELECT e FROM UserGraph e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserGraph e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

}
